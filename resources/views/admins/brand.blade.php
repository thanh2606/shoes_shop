@extends('admins.layout.master')

@section('title')
    Brand
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <a href="{{ route('brands.index') }}"><h3>Brand</h3></a>
                    <div class="x_content">
                        <button type="button" class="btn btn-success btn-xs" data-toggle="modal"
                                data-target=".bs-example-modal-lg"><i class="fa fa-plus-square"></i> Add brand
                        </button>

                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span
                                                aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Add brand</h4>
                                    </div>
                                    <div class="modal-body">
                                        {{--                                        form create brand--}}
                                        <form class="form-horizontal form-label-left" enctype="multipart/form-data"
                                              id="createForm">
                                            @csrf
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Name*</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Name of new category" name="name"
                                                           id="input-name-create-form">
                                                    <p class="error" style="color: red" id="error-name-create-form"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label
                                                    class="control-label col-md-3 col-sm-3 col-xs-12">Description*</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <textarea class="form-control" rows="3"
                                                              placeholder="Description of new category"
                                                              name="description"
                                                              id="input-description-create-form"></textarea>
                                                    <p class="error" style="color: red"
                                                       id="error-description-create-form"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Email*</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Example: example@example.com" name="email"
                                                           id="input-email-create-form">
                                                    <p class="error" style="color: red"
                                                       id="error-email-create-form"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Image*</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="file" class="form-control" name="image"
                                                           id="input-image-create-form">
                                                    <p class="error" style="color: red"
                                                       id="error-image-create-form"></p>
                                                </div>
                                                <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <img src="{{ asset('storage/no.png') }}" id="previewImage" width="200px" height="200px">
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <button type="reset" class="btn btn-primary">Reset</button>
                                                    <button type="button" class="btn btn-success" id="btnCreateBrand">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    {{--                    form search brand--}}
                    <form class="form-horizontal form-label-left input_mask" id="searchCategory"
                          action="{{ route('brands.search') }}" method="get">
                        <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                            <p>Name: </p>
                            <input type="text" class="form-control" id="inputSuccess2" placeholder="Name" name="name" value="{{ old('name') }}">
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                            <p>Description: </p>
                            <input type="text" class="form-control" id="inputSuccess3" placeholder="Description"
                                   name="description" value="{{ old('description') }}">
                        </div>

                        <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                            <p>Email: </p>
                            <input type="text" class="form-control" id="inputSuccess3" placeholder="Email"
                                   name="email" value="{{ old('email') }}">
                        </div>

                        <div class="form-group col-md-2 col-sm-2 col-xs-12 col-md-offset-3">
                            <div class="">
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success" id="btnSearch">Search</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div>
                            <h2>Brands</h2>
                        </div>
                        <div class="x_content">
                            <!-- start project list -->
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 1%">#</th>
                                    <th style="width: 20%">Name</th>
                                    <th>Image</th>
                                    <th>Description</th>
                                    <th>Email</th>
                                    <th style="width: 20%">#Edit</th>
                                </tr>
                                </thead>
                                <tbody class="tbody">
                                <?php $dem = 1 ?>
                                @foreach($brands as $brand)
{{--                                    @if($dem <= 5)--}}
                                        <tr>
                                            <th class="stt">{{$dem}}</th>
                                            <td>
                                                <a id="name{{$brand->id}}">{{$brand->name}}</a>
                                                <br/>
                                                <small>Created {{$brand->created_at}}</small>
                                            </td>
                                            <td>
                                                <img src="{{ asset("storage/$brand->image") }}" width="100px"
                                                     height="100px" id="image{{$brand->id}}">
                                            </td>
                                            <td>
                                                <ul class="list-inline">
                                                    <p id="description{{$brand->id}}">{{$brand->description}}</p>
                                                </ul>
                                            </td>
                                            <td>
                                                <a id="email{{$brand->id}}">{{$brand->email}}</a>
                                                <br/>
                                            </td>
                                            <td>
                                                <button type="button" class="editBtn btn btn-info btn-xs"
                                                        data-toggle="modal" data-target=".bs-example-modal-lg1"
                                                        data_name="{{$brand->name}}"
                                                        data_image="{{asset("storage/$brand->image") }}"
                                                        data_description="{{$brand->description}}"
                                                        data_email="{{$brand->email}}"
                                                        data_id="{{$brand->id}}" id="btnEdit{{$brand->id}}"><i
                                                        class="fa fa-pencil"></i> Edit
                                                </button>
                                                <button type="button" class="deleteBtn btn btn-danger btn-xs"
                                                        data_id="{{$brand->id}}"><i class="fa fa-trash-o"></i> Delete
                                                </button>
                                            </td>
                                        </tr>
                                        <?php $dem++; ?>
{{--                                    @endif--}}
                                @endforeach
                                </tbody>
                            </table>
                            <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span
                                                    aria-hidden="true">×</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">Edit brand</h4>
                                        </div>
                                        <div class="modal-body">
                                            {{--                                            form edit brand--}}
                                            <form class="form-horizontal form-label-left" id="editForm"
                                                  enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control"
                                                               placeholder="Name of new category" name="name"
                                                               id="input-name-edit-form">
                                                        <p class="error" style="color: red"
                                                           id="error-name-edit-form"></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Image</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="file" class="form-control" name="image"
                                                               id="input-image-edit-form">
                                                        <p class="error" style="color: red"
                                                           id="error-image-edit-form"></p>
                                                    </div>
                                                    <div class="control-label col-md-3 col-sm-3 col-xs-12"></div>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <img src="#" id="preview-Image-edit-form" width="200px"
                                                             height="200px">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <textarea class="form-control" rows="3"
                                                                  placeholder="Description of new category"
                                                                  name="description"
                                                                  id="input-description-edit-form"></textarea>
                                                        <p class="error" style="color: red"
                                                           id="error-description-edit-form"></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Email</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control"
                                                               placeholder="Example: example@example.com" name="email"
                                                               id="input-email-edit-form">
                                                        <p class="error" style="color: red"
                                                           id="error-email-edit-form"></p>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                        <button type="button" class="btn btn-primary"
                                                                data-dismiss="modal">Cancel
                                                        </button>
                                                        <button type="reset" class="btn btn-primary">Reset</button>
                                                        <button type="submit" class="btn btn-success"
                                                                id="submitEditForm" data_id="">Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end project list -->
                            <div>{{ $brands->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('build/js/brand.js') }}"></script>
@endsection
