@extends('admins.layout.master')

@section('title')
    Index
@endsection

@section('content')
    <div class="right_col" role="main" style="min-height: 1376px;">
        <div class="">
            <div class="row top_tiles">
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-th-list"></i></div>
                        <div class="count">{{ $categories->count() }}</div>
                        <h3>Category</h3>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-truck"></i></div>
                        <div class="count">{{ $brands->count() }}</div>
                        <h3>Brand</h3>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-gift"></i></div>
                        <div class="count">{{ $products->count() }}</div>
                        <h3>Product</h3>
                    </div>
                </div>
                <div class="animated flipInY col-lg-3 col-md-3 col-sm-6 col-xs-12">
                    <div class="tile-stats">
                        <div class="icon"><i class="fa fa-male"></i></div>
                        <div class="count">{{ $customers->count() }}</div>
                        <h3>customer</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div class="x_content">
                            <div class="col-md-7 col-sm-12 col-xs-12">
                                <div>
                                    <div class="x_title">
                                        <h2>Order on day</h2>
                                        <div class="col-md-3 xdisplay_inputx form-group has-feedback"
                                             style="float: right">
                                            <input type="text" class="form-control has-feedback-left" id="single_cal1"
                                                   placeholder="First Name" aria-describedby="inputSuccess2Status">
                                            <span class="fa fa-calendar-o form-control-feedback left"
                                                  aria-hidden="true"></span>
                                            <span id="inputSuccess2Status" class="sr-only">(success)</span>
                                        </div>
                                        <div class="clearfix"></div>
                                        <h3>Income: <small id="income"></small></h3>
                                    </div>
                                    <div data-spy="scroll" data-target="#myScrollspy"
                                         style="height:350px;overflow-y: scroll;padding:5px; border: 1px solid #ccc;">
                                        <table class="table table-striped projects">
                                            <thead>
                                            <tr>
                                                <th style="width: 1%">#</th>
                                                <th>Customer</th>
                                                <th>Created at</th>
                                                <th>Note</th>
                                                <th>Total cost</th>
                                            </tr>
                                            </thead>
                                            <tbody class="tbody">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 col-sm-12 col-xs-12">
                                <div>
                                    <div class="x_title">
                                        <h2>Top 5 product seller</h2>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div data-spy="scroll" data-target="#myScrollspy"
                                         style="height:396px;overflow-y: scroll;padding:5px; border: 1px solid #ccc;">
                                        <ul class="list-unstyled top_profiles scroll-view">
                                            @foreach($topProductSeller as $product)
                                                <li class="media event row">
                                                    <div class="col-lg-4">
                                                        <img src="{{ asset("storage/".$product->image_name) }}"
                                                             width="80px" height="80" style="border-radius: 40px">
                                                    </div>
                                                    <div class="col-lg-8">
                                                        <a class="title" href="#">{{ $product->name }}</a>
                                                        <p><strong>$ {{ number_format($product->price, 2) }} </strong>
                                                        </p>
                                                        <p><strong>Sell:</strong> {{ $product->number_sell }}</p>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        var customers = new Array();
        @foreach($customers as $customer)
            customers['{{$customer->id}}'] = '{{$customer->name}}';
        @endforeach
        $('#single_cal1').change(function (event) {
            let day = $(this).val();
            let data = {'day': day};
            let url = 'admin/overview/getOrdersOnDay';
            let method = 'post';
            callAjax(url, method, data)
                .done(response => {
                    var rows = $('table').find('> tbody > tr');
                    $.each(rows, function (index, value) {
                        $(value).remove();
                    });
                    let orders = response.data;
                    let dem = 1;
                    let income = 0;
                    let tr = '';
                    $.each(orders, function (index, value) {
                        tr += `<tr>
                                    <th class="stt">${dem}</th>
                                    <td class="tdName">
                                        <p><strong>${customers[value['customer_id']]}</strong></p>
                                    </td>
                                    <td>
                                        <p>${value['created_at']}</p>
                                    </td>
                                    <td>
                                        <p>${value['note']}</p>
                                    </td>
                                    <td>
                                        <p>${value['total_cost']}
                                            đ</p>
                                    </td>
                                </tr>`;
                        dem++;
                        income += value['total_cost'];
                    });
                    $('#income').text(income);
                    $('.tbody').append(tr);
                })
                .fail(error => {
                    showErrorMessage('Opp!', 'Has a error cant get data from server');
                })
        });
    </script>
@endsection
