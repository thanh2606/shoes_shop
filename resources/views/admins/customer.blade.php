@extends('admins.layout.master')

@section('title')
    Customer
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <a href="{{ route('customers.index') }}"><h3>Customer</h3></a>
                    <div class="x_content">
                        <button type="button" class="btn btn-success btn-xs createResource" data-toggle="modal"
                                data-target=".bs-example-modal-lg"><i class="fa fa-plus-square"></i> Add customer
                        </button>
                        {{--                        form create--}}
                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span
                                                aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Add customer</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal form-label-left" id="createResourceForm">
                                            @csrf

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Name*</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Name of new customer" name="name"
                                                           id="input-name-create-form">
                                                    <p class="error" style="color: red" id="error-name-create-form"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Telephone
                                                    number*</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Telephone number of new customer"
                                                           name="telephone_number"
                                                           id="input-telephone_number-create-form">
                                                    <p class="error" style="color: red"
                                                       id="error-telephone_number-create-form"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label
                                                    class="control-label col-md-3 col-sm-3 col-xs-12">Address*</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Address of new customer" name="address"
                                                           id="input-address-create-form">
                                                    <p class="error" style="color: red"
                                                       id="error-address-create-form"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label
                                                    class="control-label col-md-3 col-sm-3 col-xs-12">Note</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <textarea class="form-control" rows="3"
                                                              placeholder="Note of new customer"
                                                              name="note"></textarea>
                                                    <p class="error" style="color: red" id="errorNote"></p>
                                                </div>
                                            </div>
                                            <div class="modal-footer">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <button type="reset" class="btn btn-primary">Reset</button>
                                                    <button type="submit" class="btn btn-success" id="postResource">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="clearfix">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel" style="height: auto;">
                            <div class="x_title">
                                <a class="collapse-link"><i class="fa fa-chevron-down"></i> Search</a>
                            </div>
                            <div class="x_content" style="display: none;">
                                <br>
                                <form class="form-horizontal form-label-left input_mask"
                                      action="{{ route('customers.search') }}" method="get">
                                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                                        <p>Name: </p>
                                        <input type="text" class="form-control" id="inputSuccess2" placeholder="Name" name="name" value="{{ old('name') }}">
                                    </div>

                                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                                        <p>Telephone number: </p>
                                        <input type="text" class="form-control" id="inputSuccess3" placeholder="Telephone number"
                                               name="telephone_number" value="{{ old('telephone_number') }}">
                                    </div>

                                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                                        <p>Address: </p>
                                        <input type="text" class="form-control" id="inputSuccess3" placeholder="Address"
                                               name="address" value="{{ old('address') }}">
                                    </div>

                                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                                        <p>Note: </p>
                                        <input type="text" class="form-control" id="inputSuccess3" placeholder="Note"
                                               name="note" value = "{{ old('note') }}">
                                    </div>

                                    <div class="form-group col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                                        <center>
                                            <div class="">
                                                <button class="btn btn-primary" type="reset">Reset</button>
                                                <button type="submit" class="btn btn-success">Search</button>
                                            </div>
                                        </center>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div>
                            <h2>Customers</h2>
                        </div>
                        <div class="x_content">
                            <!-- start project list -->
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 1%">#</th>
                                    <th>Name</th>
                                    <th>Telephone number</th>
                                    <th>Address</th>
                                    <th style="width: 30%">Note</th>
                                    <th>#Edit</th>
                                </tr>
                                </thead>
                                <tbody class="tbody">
                                <?php $dem = 1 ?>
                                @foreach($customers as $customer)
                                    <tr>
                                        <th class="stt">{{$dem}}</th>
                                        <td>
                                            <a id="tr-name-{{$customer->id}}">{{$customer->name}}</a>
                                            <br/>
                                            <small>Created {{$customer->created_at}}</small>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="tr-telephone_number-{{$customer->id}}">{{$customer->telephone_number}}</p>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="tr-address-{{$customer->id}}">{{$customer->address}}</p>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="tr-note-{{$customer->id}}">{{$customer->note}}</p>
                                            </ul>
                                        </td>
                                        <td class="tdEdit">
                                            <button type="button" class="editBtn btn btn-info btn-xs"
                                                    data-toggle="modal" data-target=".bs-example-modal-lg1"
                                                    data_name="{{$customer->name}}"
                                                    data_telephone_number="{{$customer->telephone_number}}"
                                                    data_address="{{$customer->address}}"
                                                    data_note="{{ $customer->note }}"
                                                    data_id="{{$customer->id}}" id="btnEdit{{$customer->id}}"><i
                                                    class="fa fa-pencil"></i> Edit
                                            </button>
                                            <button type="button" class="deleteBtn btn btn-danger btn-xs"
                                                    data_id="{{$customer->id}}"><i class="fa fa-trash-o"></i> Delete
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $dem++; ?>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span
                                                    aria-hidden="true">×</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">Edit customer</h4>
                                        </div>
                                        <div class="modal-body">
                                            {{--                                            form edit--}}
                                            <form class="form-horizontal form-label-left" id="editResourceForm">
                                                @csrf
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control"
                                                               placeholder="Name of customer" name="name"
                                                               id="input-name-edit-form">
                                                        <p class="error" style="color: red"
                                                           id="error-name-edit-form"></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Telephone
                                                        number</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control"
                                                               placeholder="Telephone number of customer"
                                                               name="telephone_number"
                                                               id="input-telephone_number-edit-form"/>
                                                        <p class="error" style="color: red"
                                                           id="error-telephone_number-edit-form"></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Address</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control"
                                                               placeholder="Address of customer" name="address"
                                                               id="input-address-edit-form">
                                                        <p class="error" style="color: red"
                                                           id="error-address-edit-form"></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Note</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <textarea class="form-control" rows="3"
                                                              placeholder="Note of customer"
                                                              name="note" id="input-note-edit-form"></textarea>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                        <button type="button" class="btn btn-primary"
                                                                data-dismiss="modal">Cancel
                                                        </button>
                                                        <button type="reset" class="btn btn-primary">Reset</button>
                                                        <button type="submit" class="btn btn-success"
                                                                id="postEditCategory" data_id="">Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end project list -->
                            <div>{{ $customers->appends(Request::only('name', 'telephone_number', 'address', 'note'))->links() }}</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('build/js/customer.js') }}"></script>
@endsection
