@extends('admins.layout.master')

@section('title')
    Order
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <a href="{{ route('orders.index') }}"><h3>Order</h3></a>
                    <div class="x_content">
                        <a href="{{ route('orders.create') }}" class="btn btn-success btn-xs"><i
                                class="fa fa-plus-square"></i> Add order</a>
                    </div>
                </div>
            </div>

            <div class="clearfix">
                <div class="x_content">
                    <form class="form-horizontal form-label-left input_mask" id="searchCategory"
                          action="{{ route('orders.search') }}" method="get">
                        <div class="col-md-5 col-sm-5 col-xs-6 form-group has-feedback inputSearch">
                            <p>Customer: </p>
                            <input type="text" class="form-control" id="inputSuccess2" placeholder="Name of customer" name="name" value="{{ old('name') }}">
                        </div>

                        <div class="col-md-5 col-sm-5 col-xs-6 form-group has-feedback inputSearch">
                            <p>Note: </p>
                            <input type="text" class="form-control" id="inputSuccess3" placeholder="Note"
                                   name="note" value="{{ old('note') }}">
                        </div>

                        <div class="form-group col-md-2 col-sm-2 col-xs-12 col-md-offset-3">
                            <div class="">
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success" id="btnSearchCategory">Search</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div>
                            <h2>Products</h2>
                        </div>
                        <div class="x_content">
                            <!-- start project list -->
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 1%">#</th>
                                    <th>Customer</th>
                                    <th>Created at</th>
                                    <th>Note</th>
                                    <th>Total cost</th>
                                    <th>#Edit</th>
                                </tr>
                                </thead>
                                <tbody class="tbody">
                                <?php $dem = 1 ?>
                                @foreach($resources as $resource)
                                    <tr>
                                        <th class="stt">{{$dem}}</th>
                                        <td class="tdName">
                                            @if(Request::route()->getName() == 'orders.search')
                                                <p class="tr-name-{{$resource->id}}">
                                                    <strong>{{$resource->name}}</strong></p>
                                            @endif
                                            @if(Request::route()->getName() != 'orders.search')
                                                <p class="tr-name-{{$resource->id}}">
                                                    <strong>{{$resource->customer->name}}</strong></p>
                                            @endif
                                        </td>
                                        <td>
                                            <p class="tr-created_at-{{$resource->id}}">{{$resource->created_at}}</p>
                                        </td>
                                        <td>
                                            <p class="tr-note-{{$resource->id}}">{{$resource->note}}</p>
                                        </td>
                                        <td>
                                            <p class="tr-total_cost-{{$resource->id}}">{{ number_format($resource->total_cost,2) }}
                                                đ</p>
                                        </td>
                                        <td class="tdEdit">
                                            <a href="{{ route('orders.edit',['order' => $resource->id]) }}"
                                               class="editBtn btn btn-info btn-xs">
                                                <i class="fa fa-pencil"></i> Edit
                                            </a>
                                            <button type="button" class="deleteBtn btn btn-danger btn-xs"
                                                    data_id="{{ $resource->id }}"><i
                                                    class="fa fa-trash-o"></i> Delete
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $dem++; ?>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- end project list -->
                            <div>{{ $resources->appends(Request::only('name', 'note'))->links() }}</div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('build/js/order.js') }}"></script>
@endsection
