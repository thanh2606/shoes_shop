@extends('admins.layout.master')

@section('title')
    @if(Request::route()->getName() == 'orders.edit')
        Edit order
    @endif
    @if(Request::route()->getName() != 'orders.edit')
        Create order
    @endif
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <div class="url">
                        <a href="{{ route('orders.index') }}"><h3>Order</h3></a>
                        @if(Request::route()->getName() == 'orders.edit')
                            <a><h4>Edit</h4></a>
                        @endif
                        @if(Request::route()->getName() != 'orders.edit')
                            <a><h4>Create</h4></a>
                        @endif
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Infor order </h2>
                            <div class="clearfix"></div>

                        </div>
                        <div class="x_content">
                            @if(Request::route()->getName() != 'orders.edit')
                                <div class="x_content">
                                    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog"
                                         aria-hidden="true">
                                        <div class="modal-dialog modal-lg">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal"><span
                                                            aria-hidden="true">×</span>
                                                    </button>
                                                    <h4 class="modal-title" id="myModalLabel">Select customer</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <div class="clearfix">
                                                        <div class="row">
                                                            <div class="col-md-12 col-sm-12 col-xs-12">
                                                                <div class="x_panel" style="height: auto;">
                                                                    <div class="x_title">
                                                                        <a class="collapse-link"><i
                                                                                class="fa fa-chevron-down"></i>Add new
                                                                            customer</a>
                                                                    </div>
                                                                    <div class="x_content" style="display: none;">
                                                                        <br>
                                                                        <form class="form-horizontal form-label-left"
                                                                              id="createResourceForm">
                                                                            @csrf

                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="control-label col-md-3 col-sm-3 col-xs-12">Name*</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                    <input type="text" class="form-control"
                                                                                           placeholder="Name of new customer"
                                                                                           name="name"
                                                                                           id="input-name-create-form">
                                                                                    <p class="error" style="color: red"
                                                                                       id="error-name-create-form"></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="control-label col-md-3 col-sm-3 col-xs-12">Telephone
                                                                                    number*</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                    <input type="text" class="form-control"
                                                                                           placeholder="Telephone number of new customer"
                                                                                           name="telephone_number"
                                                                                           id="input-telephone_number-create-form">
                                                                                    <p class="error" style="color: red"
                                                                                       id="error-telephone_number-create-form"></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="control-label col-md-3 col-sm-3 col-xs-12">Address*</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                                                    <input type="text" class="form-control"
                                                                                           placeholder="Address of new customer"
                                                                                           name="address"
                                                                                           id="input-address-create-form">
                                                                                    <p class="error" style="color: red"
                                                                                       id="error-address-create-form"></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="form-group">
                                                                                <label
                                                                                    class="control-label col-md-3 col-sm-3 col-xs-12">Note</label>
                                                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <textarea class="form-control" rows="3"
                                                              placeholder="Note of new customer"
                                                              name="note"></textarea>
                                                                                    <p class="error" style="color: red"
                                                                                       id="errorNote"></p>
                                                                                </div>
                                                                            </div>
                                                                            <div class="modal-footer">
                                                                                <div
                                                                                    class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                                                    <button type="button"
                                                                                            class="btn btn-primary"
                                                                                            data-dismiss="modal">
                                                                                        Cancel
                                                                                    </button>
                                                                                    <button type="reset"
                                                                                            class="btn btn-primary">Reset
                                                                                    </button>
                                                                                    <button type="submit"
                                                                                            class="btn btn-success"
                                                                                            id="postResource">
                                                                                        Submit
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </form>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <table id="datatable" class="table table-striped table-bordered">
                                                        <thead>
                                                        <tr>
                                                            <th style="width: 1%">#</th>
                                                            <th>Name</th>
                                                            <th>Telephone number</th>
                                                            <th>Address</th>
                                                            <th>Note</th>
                                                            <th>#Edit</th>
                                                        </tr>
                                                        </thead>
                                                        <tbody class="tbody">
                                                        <?php $dem = 1 ?>
                                                        @foreach($customers as $customer)
                                                            <tr>
                                                                <th class="stt">{{$dem}}</th>
                                                                <td>
                                                                    <a id="tr-name-{{$customer->id}}">{{$customer->name}}</a>
                                                                    <br/>
                                                                    <small>Created {{$customer->created_at}}</small>
                                                                </td>
                                                                <td>
                                                                    <ul class="list-inline">
                                                                        <p id="tr-telephone_number-{{$customer->id}}">{{$customer->telephone_number}}</p>
                                                                    </ul>
                                                                </td>
                                                                <td>
                                                                    <ul class="list-inline">
                                                                        <p id="tr-address-{{$customer->id}}">{{$customer->address}}</p>
                                                                    </ul>
                                                                </td>
                                                                <td>
                                                                    <ul class="list-inline">
                                                                        <p id="tr-note-{{$customer->id}}">{{$customer->note}}</p>
                                                                    </ul>
                                                                </td>
                                                                <td class="tdEdit">
                                                                    <button type="button"
                                                                            class="selectCustomerBtn btn btn-success btn-xs"
                                                                            data_name="{{ $customer->name }}"
                                                                            data_address="{{ $customer->address }}"
                                                                            data_telephone=" {{ $customer->telephone_number }}"
                                                                            data_note="{{ $customer->note }}"
                                                                            data_id="{{$customer->id}}"> Select
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            <?php $dem++; ?>
                                                        @endforeach
                                                        </tbody>
                                                    </table>
                                                </div>
                                                <div class="modal-footer">
                                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                        <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                            Cancel
                                                        </button>
                                                        <button type="submit" class="btn btn-success" id="selectCustomer">
                                                            Add customer
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif
                            <br>
                            <div class="x_content">
                                <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                                    @if(Request::route()->getName() != 'orders.edit')
                                        <button id="selectBtn" type="button" class="btn btn-success btn-xs"
                                                data-toggle="modal"
                                                data-target=".bs-example-modal-lg"><i class="fa fa-plus-square"></i> Select
                                            customer
                                        </button>
                                    @endif
                                    <p class="error-customer" style="color: red"></p>
                                    <h2>customer</h2>
                                    @if(Request::route()->getName() == 'orders.edit')
                                        <h4 id="infor-name">{{ $order->customer->name }}</h4>
                                        <ul class="list-unstyled user_data">
                                            <li><i id="infor-address" class="fa fa-map-marker user-profile-icon">
                                                    {{ $order->customer->address }}</i></li>
                                            <li><i id="infor-telephone_number"
                                                   class="fa fa-tty"> {{ $order->customer->telephone_number }}</i></li>
                                            <li><i id="infor-note"
                                                   class="fa fa-pencil-square-o"> {{ $order->customer->note }}</i></li>
                                        </ul>
                                        <br>
                                        <h3><i class="fa fa-money"></i> Total cost: <small id="infor-total-cost"
                                                                                           data_total_cost='{{ $order->total_cost }}'>${{ number_format($order->total_cost , 2)}}</small>
                                        </h3>
                                        <p class="error-total_cost" style="color: red"></p>
                                    @endif
                                    @if(Request::route()->getName() != 'orders.edit')
                                        <h4 id="infor-name">Name of customer?</h4>
                                        <ul class="list-unstyled user_data">
                                            <li><i id="infor-address" class="fa fa-map-marker user-profile-icon">
                                                    Address...</i></li>
                                            <li><i id="infor-telephone_number" class="fa fa-tty"> Telephone...</i></li>
                                            <li><i id="infor-note" class="fa fa-pencil-square-o"> Note...</i></li>
                                        </ul>
                                        <br>
                                        <h3><i class="fa fa-money"></i> Total cost: <small id="infor-total-cost"
                                                                                           data_total_cost=''></small>
                                        </h3>
                                        <p class="error-total_cost" style="color: red"></p>
                                    @endif

                                </div>
                                <div class="col-md-9 col-sm-9 col-xs-12" style="border-left: 5px solid #eee;">
                                    <button type="button" class="editBtn btn btn-info btn-xs"
                                            data-toggle="modal" data-target=".bs-example-modal-lg1"><i
                                            class="fa fa-pencil"></i> Add product
                                    </button>
                                    <p class="error-products" style="color: red"></p>
                                    <div class="x_content">
                                        <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog"
                                             aria-hidden="true">
                                            <div class="modal-dialog modal-lg">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal"><span
                                                                aria-hidden="true">×</span>
                                                        </button>
                                                        <h4 class="modal-title" id="myModalLabel">Select product</h4>
                                                    </div>
                                                    <div class="modal-body">
                                                        <table id="datatable1"
                                                               class="table table-striped table-bordered">
                                                            <thead>
                                                            <tr>
                                                                <th style="width: 1%">#</th>
                                                                <th>Name</th>
                                                                <th>Image</th>
                                                                <th>Infor</th>
                                                                <th>#Edit</th>
                                                            </tr>
                                                            </thead>
                                                            <tbody class="tbody tbodyProduct">
                                                            <?php $dem = 1 ?>
                                                            @foreach($products as $product)
                                                                <tr>
                                                                    <th class="stt">{{$dem}}</th>
                                                                    <td class="tdName">
                                                                        <p><strong>{{$product->name}}</strong></p>
                                                                        <p>
                                                                            <small>Created {{$product->created_at}}</small>
                                                                        </p>
                                                                    </td>
                                                                    <td class="tdImage">
                                                                        <img
                                                                            src="{{ asset("storage/".$product->images()->where('status', 1)->get()[0]->name) }}"
                                                                            width="100px"
                                                                            height="100px">
                                                                    </td>
                                                                    <td class="tdInfor">
                                                                        <p>
                                                                            <strong>Category:</strong> {{ $product->category->name }}
                                                                        </p>
                                                                        <p>
                                                                            <strong>Brand:</strong> {{ $product->brand->name }}
                                                                        </p>
                                                                        <p>
                                                                            <strong>Inventory:</strong> {{ $product->number_of }}
                                                                        </p>
                                                                        <p>
                                                                            <strong>Price:</strong> {{ number_format($product->price,2) }}
                                                                            đ</p>
                                                                    </td>
                                                                    <td class="tdEdit">
                                                                        @if($product->number_of != 0)
                                                                            <button type="button"
                                                                                    class="selectProductBtn btn btn-success btn-xs"
                                                                                    data_name="{{ $product->name }}"
                                                                                    data_number_of="{{ $product->number_of }}"
                                                                                    data_price="{{ $product->price }}"
                                                                                    data_id="{{$product->id}}"> Select
                                                                            </button>
                                                                        @endif
                                                                        @if($product->number_of == 0)
                                                                            <button type="button"
                                                                                    class="btn btn-error btn-xs"
                                                                                    data_name="{{ $product->name }}"
                                                                                    data_number_of="{{ $product->number_of }}"
                                                                                    data_id="{{$product->id}}"> dont
                                                                                have
                                                                            </button>
                                                                        @endif
                                                                    </td>
                                                                </tr>
                                                                <?php $dem++; ?>
                                                            @endforeach
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                            <button type="button" class="btn btn-primary"
                                                                    data-dismiss="modal">
                                                                Cancel
                                                            </button>
                                                            <button type="submit" class="btn btn-success"
                                                                    id="addProducts">
                                                                Add products
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12">
                                        <div class="x_panel">
                                            <div class="x_title">
                                                <h2>Products <small>list product on order</small></h2>
                                                <div class="clearfix"></div>
                                            </div>
                                            <div class="x_content">
                                                <table class="table">
                                                    <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Name</th>
                                                        <th>Number of</th>
                                                        <th>#Edit</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody class="tbodyMain">
                                                    @if(Request::route()->getName() == 'orders.edit')
                                                        <?php $dem = 1 ?>
                                                        @foreach($order->products()->get() as $product)
                                                            <tr>
                                                                <th scope="row">{{ $dem }}</th>
                                                                <td>{{ $product->name }}</td>
                                                                <td class="input_number_of"><input type="number"
                                                                                                   class="form-control input-number_of-create-form"
                                                                                                   name="number_of"
                                                                                                   data_cost='{{ $product->price }}'
                                                                                                   data_id='{{ $product->id }}'
                                                                                                   data_name='{{ $product->name }}'
                                                                                                   value="{{ $product->pivot->number_of_product }}">
                                                                </td>
                                                                <td>
                                                                    <button type="button"
                                                                            class="removeProductBtn btn btn-warning btn-xs"
                                                                            data_id='{{ $product->id }}'
                                                                            data_key='{{ $product->id }}'
                                                                    > Remove
                                                                    </button>
                                                                </td>
                                                            </tr>
                                                            <?php $dem++; ?>
                                                        @endforeach
                                                    @endif
                                                    </tbody>
                                                </table>

                                            </div>
                                        </div>
                                    </div>
                                    @if(Request::route()->getName() == 'orders.edit')
                                        <div>
                                            <div class="form-group">
                                                <label class="control-label">
                                                    <h>Note</h>
                                                </label>
                                                <div class="">
                                                    <textarea class="form-control" rows="3"
                                                              placeholder="Note of new order"
                                                              id="inputNote">{{ $order->note }}</textarea>
                                                    <p class="error" style="color: red" id="errorNote"></p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                    @if(Request::route()->getName() != 'orders.edit')
                                        <div>
                                            <div class="form-group">
                                                <label class="control-label">
                                                    <h>Note</h>
                                                </label>
                                                <div class="">
                                                    <textarea class="form-control" rows="3"
                                                              placeholder="Note of new order" id="inputNote"></textarea>
                                                    <p class="error" style="color: red" id="errorNote"></p>
                                                </div>
                                            </div>
                                        </div>
                                    @endif
                                </div>

                            </div>
                            <div class="form-group">
                                <center>
                                    <div>
                                        <button type="button" class="btn btn-primary">Cancel</button>
                                        <button type="button" id="createOrderBtn" class="btn btn-success">Submit
                                        </button>
                                    </div>
                                </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        //VARIABLE
        var customerData;
        var products = new Array();
        var dataRequest;

        //function clear message error when open modal
        $('.createResource').click(function () {
            $('.error').text('');
        });

        //function append table
        function prepend(response) {
            let tbody = $('.tbody');
            let name = response.data.name;
            let created_at = response.data.created_at;
            let telephone_number = response.data.telephone_number;
            let address = response.data.address;
            let note = response.data.note;
            let id = response.data.id;
            let tr = `<tr>
                                        <th class="stt"></th>
                                        <td>
                                            <a id="tr-name-${id}">${name}</a>
                                            <br/>
                                            <small>Created ${created_at}</small>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="tr-telephone_number-${id}">${telephone_number}</p>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="tr-address-${id}">${address}</p>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="tr-note-${id}">${note}</p>
                                            </ul>
                                        </td>
                                        <td class="tdEdit">
                                            <button type="button"
                                                    class="selectCustomerBtn btn btn-success btn-xs"
                                                    data_name="${name}"
                                                    data_address="${address}"
                                                    data_telephone="${telephone_number}"
                                                    data_note="${note}"
                                                    data_id="${id}"> Select
                                            </button>
                                        </td>
                                    </tr>`;
            tbody.prepend(tr);
        }

        //function reload tr of tableBody
        function reloadOrderOfTableBody() {
            $("tr .stt").each(function (i) {
                $(this).text(i + 1);
                i++;
            });
        }

        //CREATE CUSTOMER
        $('#postResource').click(function (event) {
            event.preventDefault();
            let dataResource = $('#createResourceForm').serialize();
            let urlResource = '/admin/customers';
            callAjax(urlResource, 'post', dataResource)
                .done(response => {
                    Swal.fire({
                        icon: 'success',
                        title: 'OK',
                        text: 'Create customer successful',
                    }).then((result) => {
                        if (result.value) {
                            prepend(response);
                            reloadOrderOfTableBody();
                            if ($('tbody tr').length == 6) {
                                $('tbody tr:last-child').remove();
                            }
                        }
                    })
                })
                .fail(error => {
                    show_error_and_focus_on_form('-create-form', error.responseJSON.errors);
                    showErrorMessage('Opp!', 'Create category fail');
                })
        });

        //Select customer
        $(document).on('click', '.selectCustomerBtn', function (event) {
            $('.error-customer').text('');
            $('.removeCustomerBtn').removeClass('removeCustomerBtn').removeClass('btn-warning').addClass('selectCustomerBtn').addClass('btn-success').text('Select');
            $(this).removeClass('selectCustomerBtn').removeClass('btn-success').addClass('removeCustomerBtn').addClass('btn-warning').text('Remove');
            customerData = {
                "id": $(this).attr('data_id'),
                "name": $(this).attr('data_name'),
                "address": $(this).attr('data_address'),
                "telephone_number": $(this).attr('data_telephone'),
                "note": $(this).attr('data_note')
            }
        });

        $(document).on('click', '.removeCustomerBtn', function (event) {
            $(this).removeClass('removeCustomerBtn').removeClass('btn-warning').addClass('selectCustomerBtn').addClass('btn-success').text('Select');
            customerData = {};
            $('#infor-name').text('Name of customer?');
            $('#infor-address').text(' Address...');
            $('#infor-telephone_number').text(' Telephone...');
            $('#infor-note').text(' Note...');
        });

        $('#selectCustomer').click(function () {
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Add customer successful',
                showConfirmButton: false,
                timer: 1000
            });
            $('#infor-name').text(' ' + customerData.name);
            $('#infor-address').text(' ' + customerData.address);
            $('#infor-telephone_number').text(' ' + customerData.telephone_number);
            $('#infor-note').text(' ' + customerData.note);
        });

        //add product
        $('#datatable1').DataTable();

        //add data product to arr products
        $(document).on('click', '.selectProductBtn', function (event) {
            $(this).removeClass('selectProductBtn').removeClass('btn-success').addClass('removeProductBtn-'+$(this).attr('data_id')).addClass('selected').addClass('btn-warning').text('Remove');
        });

        //function calculate total cost
        function calculate() {
            let total_cost = 0;
            $('.tbodyMain  > tr').each(function (index, tr) {
                let input = $(tr).children().children('.input-number_of-create-form');
                if (input.val() >= 0)
                    total_cost += input.val() * input.attr('data_cost');
                else total_cost = null;
            });
            $('#infor-total-cost').text(numeral(total_cost).format('$0,0.00'));
            $('#infor-total-cost').attr('data_total_cost', total_cost);
        }

        const isEmpty = v => {
            return Object.keys(v).length === 0;
        };

        //add tr after select product
        $('#addProducts').click(function () {
            $('.error-products').text('');
            $('.error-total_cost').text('');
            let dem = products.length;
            $('.tbodyProduct  > tr').each(function (index, tr) {
                if($(tr).find('.selected').attr('data_price') !== undefined) {
                    products[dem] = {
                        'id': $(tr).find('.btn-warning').attr('data_id'),
                        'name': $(tr).find('.btn-warning').attr('data_name'),
                        'price': $(tr).find('.btn-warning').attr('data_price'),
                        'inventory': $(tr).find('.btn-warning').attr('data_number_of')
                    };
                    dem++;
                }
            });
            let count = 1;
            products.forEach(function (value, i) {
                let tr = `<tr>
                                                        <th scope="row">${count}</th>
                                                        <td>${value.name}</td>
                                                        <td class="input_number_of"><input type="number" class="form-control input-number_of-create-form"
                                                                                       name="number_of"
                                                                                       data_cost = '${value.price}'
                                                                                       data_id="${value.id}"
                                                                                       data_name = "${value.name}"
                                                                                       value="1"></td>
                                                        <td><button type="button"
                                                                        class="removeProductBtn btn btn-warning btn-xs"
                                                                        data_id="${value.id}"
                                                                        data_key = '${i}'
                                                                        > Remove
                                                                </button></td>
                                                    </tr>`;
                $('.tbodyMain').append(tr);
                count ++;
            });
            $('.selected').removeClass('selected');
            products = new Array();
            calculate();
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: 'Your products has been added',
                showConfirmButton: false,
                timer: 1000
            });
        });

        @if(Request::route()->getName() != 'orders.edit')
            //remove product from order
            $(document).on('click', '.removeProductBtn', function (event) {
                let className = '.removeProductBtn-'+$(this).attr('data_id');
                $(className).removeClass(className).removeClass('btn-warning').addClass('selectProductBtn').addClass('btn-success').text('select');
                $(this).parent().parent().remove();
                calculate();
            });
        @endif
        @if(Request::route()->getName() == 'orders.edit')
            //remove product from order
            $(document).on('click', '.removeProductBtn', function (event) {
                confirmCreate();
                $(this).parent().parent().remove();
                calculate();
            });
        @endif

        //change total cost when number of category change
        $(document).on('change', '.input-number_of-create-form', function (event) {
            calculate();
        });


        @if(Request::route()->getName() == 'orders.edit')
            //click submit to create order
            $('#createOrderBtn').click(function () {
                let url = '../{{ request()->order }}';
                let method = 'patch';
                Swal.fire({
                    title: 'Are you sure?',
                    text: "Your order will be storage!",
                    icon: 'question',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes'
                }).then((result) => {
                    if (result.value) {
                        confirmCreate();
                        callAjax(url, method, dataRequest)
                            .done(response => {
                                Swal.fire(
                                    'Successful!',
                                    response.message,
                                    'success'
                                )
                            })
                            .fail(error => {
                                let errorString = '';
                                $.each(error.responseJSON.data, function (key, value) {
                                    $('.error-' + key).text(value);
                                });
                                showErrorMessage(error.responseJSON.message, errorString);
                            })
                    }
                })
            });

            //after confirm create order
            function confirmCreate() {
                let products = new Array();
                dem = 0;
                $('.tbodyMain  > tr').each(function (index, tr) {
                    let input = $(tr).children().children('.input-number_of-create-form');
                    products[dem] = {
                        'id': input.attr('data_id'),
                        'name': input.attr('data_name'),
                        'number_of': input.val(),
                    };
                    dem++;
                });
                let total_cost = $('#infor-total-cost').attr('data_total_cost');
                dataRequest = {
                    'total_cost': total_cost,
                    'note': $('#inputNote').val(),
                    'products': products
                };
            }
        @endif
        @if(Request::route()->getName() != 'orders.edit')
        //click submit to create order
        $('#createOrderBtn').click(function () {
            let url = '../orders';
            let method = 'post';
            Swal.fire({
                title: 'Are you sure?',
                text: "Your order will be storage!",
                icon: 'question',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes'
            }).then((result) => {
                if (result.value) {
                    confirmCreate();
                    callAjax(url, method, dataRequest)
                        .done(response => {
                            Swal.fire(
                                'Successful!',
                                response.message,
                                'success'
                            )
                        })
                        .fail(error => {
                            let errorString = '';
                            $.each(error.responseJSON.data, function (key, value) {
                                $('.error-' + key).text(value);
                            });
                            showErrorMessage(error.responseJSON.message, errorString);
                        })
                }
            })
        });

        //after confirm create order
        function confirmCreate() {
            let products = new Array();
            dem = 0;
            $('.tbodyMain  > tr').each(function (index, tr) {
                let input = $(tr).children().children('.input-number_of-create-form');
                products[dem] = {
                    'id': input.attr('data_id'),
                    'name': input.attr('data_name'),
                    'number_of': input.val(),
                };
                dem++;
            });
            let total_cost = $('#infor-total-cost').attr('data_total_cost');
            dataRequest = {
                'customer': customerData,
                'total_cost': total_cost,
                'note': $('#inputNote').val(),
                'products': products
            };
        }
        @endif
    </script>
@endsection
