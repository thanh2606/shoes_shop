@extends('admins.layout.master')

@section('title')
    Product
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <a href="{{ route('products.index') }}"><h3>Product</h3></a>
                    <div class="x_content">
                        <a href="{{ route('products.create') }}" class="btn btn-success btn-xs"><i
                                class="fa fa-plus-square"></i> Add product</a>
                    </div>
                </div>
                <div class="x_content">
                </div>
            </div>

            <div class="clearfix">
                <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <div class="x_panel" style="height: auto;">
                            <div class="x_title">
                                <a class="collapse-link"><i class="fa fa-chevron-down"></i> Search</a>
                            </div>
                            <div class="x_content" style="display: none;">
                                <br>
                                <form class="form-horizontal form-label-left input_mask"
                                      action="{{ route('products.search') }}" method="get">
                                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                                        <p>Name: </p>
                                        <input type="text" class="form-control" placeholder="Name..." name="name" value="{{ old('name') }}">
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                                        <p>Description: </p>
                                        <input type="text" class="form-control" placeholder="Description..."
                                               name="description" value="{{ old('description') }}">
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                                        <p>Category: </p>
                                        <input type="text" class="form-control" placeholder="Category..."
                                               name="category" value="{{ old('category') }}">
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                                        <p>Brand: </p>
                                        <input type="text" class="form-control" placeholder="Brand..."
                                               name="brand" value="{{ old('brand') }}">
                                    </div>
                                    <div class="col-md-3 col-sm-3 col-xs-3 form-group has-feedback inputSearch">
                                        <p>Soft delete: </p>
                                        <input type="checkbox" class="form-control" placeholder="Brand..."
                                               name="softDelete[]" value="true" {{ old('softDelete')[0] ? 'checked' : null }}>
                                    </div>
                                    <div class="form-group col-md-12 col-sm-12 col-xs-12 col-md-offset-3">
                                        <div class="">
                                            <button class="btn btn-primary" type="reset">Reset</button>
                                            <button type="submit" class="btn btn-success" id="btnSearch">Search</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div>
                            <h2>Products</h2>
                        </div>
                        <div class="x_content">
                            <!-- start project list -->
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 1%">#</th>
                                    <th>Name</th>
                                    <th>Image</th>
                                    <th style="width: 30%">Description</th>
                                    <th>Infor</th>
                                    <th>Soft deleted</th>
                                    <th>#Edit</th>
                                </tr>
                                </thead>
                                <tbody class="tbody">
                                <?php $dem = 1 ?>
                                @foreach($resources as $resource)
                                    <tr>
                                        <th class="stt">{{$dem}}</th>
                                        <td class="tdName">
                                            <p><strong>{{$resource->name}}</strong></p>
                                            <p><small>Created {{$resource->created_at}}</small></p>
                                        </td>
                                        <td class="tdImage">
                                            @if(Request::route()->getName() != 'products.search')
                                                <img
                                                    src="{{ asset("storage/".$resource->images()->where('status', 1)->get()[0]->name) }}"
                                                    width="100px"
                                                    height="100px">
                                            @endif
                                            @if(Request::route()->getName() == 'products.search')
                                                <img
                                                    src="{{ asset("storage/".$resource->image_name) }}"
                                                    width="100px"
                                                    height="100px">
                                            @endif
                                        </td>
                                        <td class="tdDescription">
                                            <p>{{$resource->description}}</p>
                                        </td>
                                        <td class="tdInfor">
                                            @if(Request::route()->getName() != 'products.search')
                                                <p><strong>Category:</strong> {{ $resource->category->name }}</p>
                                                <p><strong>Brand:</strong> {{ $resource->brand->name }}</p>
                                            @endif
                                            @if(Request::route()->getName() == 'products.search')
                                                <p><strong>Category:</strong> {{ $resource->category_name }}</p>
                                                <p><strong>Brand:</strong> {{ $resource->brand_name }}</p>
                                            @endif
                                            <p><strong>Inventory:</strong> {{ $resource->number_of }}</p>
                                            <p><strong>Price:</strong> {{ number_format($resource->price,2) }} đ</p>
                                        </td>
                                        <td class="tdSoftDelete">
                                            <label>
                                                @if($resource->deleted_at == null)
                                                    <button type="button"
                                                            class="softDeleteBtn btn btn-success btn-xs"
                                                            data_id="{{ $resource->id }}"><i
                                                            class="fa fa-trash-o"></i> Move to trash
                                                    </button>
                                                @endif
                                                @if($resource->deleted_at != null)
                                                    <button type="button"
                                                            class="restoreBtn btn btn-warning btn-xs"
                                                            data_id="{{ $resource->id }}"><i
                                                            class="fa fa-trash-o"></i> Restore
                                                    </button>
                                                @endif
                                            </label>
                                        </td>
                                        <td class="tdEdit">
                                            <a href="{{ route('products.edit',['product' => $resource->id]) }}"
                                               class="editBtn btn btn-info btn-xs">
                                                <i class="fa fa-pencil"></i> Edit
                                            </a>
                                            <button type="button" class="deleteBtn btn btn-danger btn-xs"
                                                    data_id="{{ $resource->id }}"><i
                                                    class="fa fa-trash-o"></i> Delete
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $dem++; ?>
                                @endforeach
                                </tbody>
                            </table>
                            <!-- end project list -->
                            <?php
                            $name = (Request::get('name') == null) ? '' : Request::get('name');
                            $description = (Request::get('description') == null) ? '' : Request::get('description');
                            $brand = (Request::get('brand') == null) ? '' : Request::get('brand');
                            $category = (Request::get('category') == null) ? '' : Request::get('category');
                            $softDelete = (Request::get('softDelete') == null) ? '' : Request::get('softDelete');
                            $data = [
                                'name' => $name,
                                'description' => $description,
                                'category' => $category,
                                'brand' => $brand,
                                'softDelete' => $softDelete
                            ];
                            ?>
                            @if(Request::route()->getName() == 'products.search')
                                <div>{{ $resources->appends($data)->links() }}</div>
                            @endif
                            @if(Request::route()->getName() != 'products.search')
                                <div>{{ $resources->links() }}</div>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('build/js/product.js') }}"></script>
@endsection
