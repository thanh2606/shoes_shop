@extends('admins.layout.master')

@section('title')
    Edit product
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <div class="url">
                        <a href="{{ route('products.index') }}"><h3>Product</h3></a>
                    </div>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12 col-xs-12">
                    <div class="x_panel">
                        <div class="x_title">
                            <h2>Infor product </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="x_content row">
                            <br>
                            <?php
                            if (Session::has('errors'))
                                $error = Session::get('errors')->default->messages();
                            ?>
                            <form class="col-lg-6 col-md-6 col-xs-6 form-horizontal form-label-left" action="{{ route('products.update', ['product' => $idProduct]) }}"
                                  method="post">
                                @csrf
                                <input name="_method" type="hidden" value="PUT">
                                <div class="row">
                                    <div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Category</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" name="category" id="input-category">
                                                    @foreach($categories as $category)
                                                        <option
                                                            value="{{ $category->id }}"
                                                            @if($resource->category->id == $category->id) selected='selected' @endif>{{ $category->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if(isset($error['category']))
                                                    <div>
                                                        <p class="errorMessage">{{ $error['category'][0] }}</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Brand</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <select class="form-control" name="brand" id="input-brand">
                                                    @foreach($brands as $brand)
                                                        <option value="{{ $brand->id }}"
                                                                @if($resource->brand->id == $brand->id) selected='selected' @endif>{{ $brand->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if(isset($error['brand']))
                                                    <div>
                                                        <p class="errorMessage">{{ $error['brand'][0] }}</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input type="text" class="form-control" placeholder="Default Input"
                                                       name="name" id="input-name" value="{{ $resource->name }}">
                                                @if(isset($error['name']))
                                                    <div>
                                                        <p class="errorMessage">{{ $error['name'][0] }}</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <textarea class="form-control" rows="3"
                                                          placeholder="description of new product"
                                                          name="description"
                                                          id="input-description">{{ $resource->description }}</textarea>
                                                @if(isset($error['description']))
                                                    <div>
                                                        <p class="errorMessage">{{ $error['description'][0] }}</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Number of
                                                product</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input type="number" class="form-control"
                                                       name="number_of" value="{{ $resource->number_of }}">
                                                @if(isset($error['number_of']))
                                                    <div>
                                                        <p class="errorMessage">{{ $error['number_of'][0] }}</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label col-md-3 col-sm-3 col-xs-12">Price (đ)</label>
                                            <div class="col-md-9 col-sm-9 col-xs-12">
                                                <input type="number" class="form-control"
                                                       name="price" value="{{ $resource->price }}">
                                                @if(isset($error['price']))
                                                    <div>
                                                        <p class="errorMessage">{{ $error['price'][0] }}</p>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <center>
                                        <div>
                                            <button type="reset" class="btn btn-primary">Reset</button>
                                            <button type="submit" class="btn btn-success">Save</button>
                                        </div>
                                    </center>
                                </div>
                            </form>
                            <div class="col-lg-6 col-md-6 col-xs-6">
                                <div class="form-group">
                                    <label class="control-label col-md-3 col-sm-3 col-xs-12">Add images</label>
                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                        <input type="file" class="form-control" name="images[]"
                                               id="input-image-edit-form" multiple id="input-images"
                                               value="{{ old('images') }}" product_id="{{ $resource->id }}">
                                        @if(isset($error['images']))
                                            <div>
                                                <p class="errorMessage">{{ $error['images'][0] }}</p>
                                            </div>
                                        @endif
                                        @if(isset($error['images.0']))
                                            <div>
                                                <p class="errorMessage">{{ $error['images.0'][0] }}</p>
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-12 col-sm-12 col-xs-12 row">
                                        <div class="form-group" id="preview-list-image">
                                            @foreach($resource->images()->get() as $image)
                                                <div class="col-md-4 col-sm-4 col-xs-4 showImage">
                                                    <img class="img-in-preview-list-edit-form @if($image->status == 1) isMain @endif" src="{{ asset('storage/'.$image->name) }}" data_id = '{{ $image->id }}'>
                                                    @if($image->status != 1) <button class="btn btn-success btnSetMainImage" data_id = "{{ $image->id }}">Set main image</button> @endif
                                                    @if($image->status == 1) <p class="mainImage">MAIN IMAGE</p> @endif
                                                    @if($image->status != 1) <i class="btnDeleteImage fa fa-times-circle" data_id = "{{ $image->id }}"></i> @endif
                                                </div>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('build/js/product.js') }}"></script>
    @if(Session::has('successful'))
        <script>
            Swal.fire({
                position: 'top',
                icon: 'success',
                title: '{{ Session::pull('successful') }}',
                showConfirmButton: false,
                timer: 1500
            })
        </script>
    @endif
@endsection
