@extends('admins.layout.master')

@section('title')
    index
@endsection

@section('content')
    <div class="right_col" role="main">
        <div class="">
            <div class="page-title">
                <div class="title_left">
                    <a href="{{ route('categories.index') }}"><h3>Category</h3></a>
                    <div class="x_content">
                        <button type="button" class="showFormCreateBtn btn btn-success btn-xs" data-toggle="modal"
                                data-target=".bs-example-modal-lg"><i class="fa fa-plus-square"></i> Add category
                        </button>

                        <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                            <div class="modal-dialog modal-lg">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close" data-dismiss="modal"><span
                                                aria-hidden="true">×</span>
                                        </button>
                                        <h4 class="modal-title" id="myModalLabel">Add category</h4>
                                    </div>
                                    <div class="modal-body">
                                        <form class="form-horizontal form-label-left" id="createResourceForm">
                                            @csrf

                                            <div class="form-group">
                                                <label class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <input type="text" class="form-control"
                                                           placeholder="Name of new category" name="name" id="input-name-create-form">
                                                    <p class="error" style="color: red" id="error-name-create-form"></p>
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label
                                                    class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                                <div class="col-md-9 col-sm-9 col-xs-12">
                                                    <textarea class="form-control" rows="3"
                                                              placeholder="Description of new category"
                                                              name="description" id="input-description-create-form"></textarea>
                                                    <p class="error" style="color: red" id="error-description-create-form"></p>
                                                </div>
                                            </div>

                                            <div class="modal-footer">
                                                <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                    <button type="button" class="btn btn-primary" data-dismiss="modal">
                                                        Cancel
                                                    </button>
                                                    <button type="reset" class="btn btn-primary">Reset</button>
                                                    <button type="submit" class="btn btn-success" id="postCategory">
                                                        Submit
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="x_content">
                    <form class="form-horizontal form-label-left input_mask" id="searchCategory"
                          action="{{ route('category.search') }}" method="get">
                        <div class="col-md-5 col-sm-5 col-xs-6 form-group has-feedback inputSearch">
                            <p>Name: </p>
                            <input type="text" class="form-control" id="inputSuccess2" placeholder="Name" name="name" value="{{ old('name') }}">
                        </div>

                        <div class="col-md-5 col-sm-5 col-xs-6 form-group has-feedback inputSearch">
                            <p>Description: </p>
                            <input type="text" class="form-control" id="inputSuccess3" placeholder="Description"
                                   name="description" value="{{ old('description') }}">
                        </div>

                        <div class="form-group col-md-2 col-sm-2 col-xs-12 col-md-offset-3">
                            <div class="">
                                <button class="btn btn-primary" type="reset">Reset</button>
                                <button type="submit" class="btn btn-success" id="btnSearchCategory">Search</button>
                            </div>
                        </div>

                    </form>
                </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
                <div class="col-md-12">
                    <div class="x_panel">
                        <div>
                            <h2>Categories</h2>
                        </div>
                        <div class="x_content">
                            <!-- start project list -->
                            <table class="table table-striped projects">
                                <thead>
                                <tr>
                                    <th style="width: 1%">#</th>
                                    <th style="width: 20%">Name</th>
                                    <th>Description</th>
                                    <th style="width: 20%">#Edit</th>
                                </tr>
                                </thead>
                                <tbody class="tbody">
                                <?php $dem = 1 ?>
                                @foreach($categories as $category)
                                    <tr>
                                        <th class="stt">{{$dem}}</th>
                                        <td>
                                            <a id="name{{$category->id}}">{{$category->name}}</a>
                                            <br/>
                                            <small>Created {{$category->created_at}}</small>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="description{{$category->id}}">{{$category->description}}</p>
                                            </ul>
                                        </td>
                                        <td>
                                            <button type="button" class="editCategoryBtn btn btn-info btn-xs"
                                                    data-toggle="modal" data-target=".bs-example-modal-lg1"
                                                    data_name="{{$category->name}}"
                                                    data_description="{{$category->description}}"
                                                    data_id="{{$category->id}}" id="btnEdit{{$category->id}}"><i
                                                    class="fa fa-pencil"></i> Edit
                                            </button>
                                            <button type="button" class="deleteCategoryBtn btn btn-danger btn-xs"
                                                    data_id="{{$category->id}}"><i class="fa fa-trash-o"></i> Delete
                                            </button>
                                        </td>
                                    </tr>
                                    <?php $dem++; ?>
                                @endforeach
                                </tbody>
                            </table>
                            <div class="modal fade bs-example-modal-lg1" tabindex="-1" role="dialog" aria-hidden="true">
                                <div class="modal-dialog modal-lg">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal"><span
                                                    aria-hidden="true">×</span>
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">Edit category</h4>
                                        </div>
                                        <div class="modal-body">
                                            <form class="form-horizontal form-label-left" id="editResourceForm">
                                                @csrf
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Name</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <input type="text" class="form-control"
                                                               placeholder="Name of new category" name="name"
                                                               id="input-name-edit-form">
                                                        <p class="errorEdit" style="color: red" id="error-name-edit-form"></p>
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label
                                                        class="control-label col-md-3 col-sm-3 col-xs-12">Description</label>
                                                    <div class="col-md-9 col-sm-9 col-xs-12">
                                                        <textarea class="form-control" rows="3"
                                                                  placeholder="Description of new category"
                                                                  name="description"
                                                                  id="input-description-edit-form"></textarea>
                                                        <p class="errorEdit" style="color: red"
                                                           id="error-description-edit-form"></p>
                                                    </div>
                                                </div>

                                                <div class="modal-footer">
                                                    <div class="col-md-9 col-sm-9 col-xs-12 col-md-offset-3">
                                                        <button type="button" class="btn btn-primary"
                                                                data-dismiss="modal">Cancel
                                                        </button>
                                                        <button type="reset" class="btn btn-primary">Reset</button>
                                                        <button type="submit" class="btn btn-success"
                                                                id="postEditCategory" data_id="">Submit
                                                        </button>
                                                    </div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- end project list -->
                            <div>{{ $categories->appends(Request::only('name', 'description'))->links() }}</div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script src="{{ asset('build/js/category.js') }}"></script>
@endsection
