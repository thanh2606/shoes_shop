<?php

namespace App;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'name',
        'description',
        'number_of',
        'price',
        'product_image_id',
        'brand_id',
        'category_id',
    ];

    public function category()
    {
        return $this->belongsTo('App\Category');
    }

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }

    public function images()
    {
        return $this->hasMany('App\Image');
    }

    public function orders()
    {
        return $this->belongsToMany('App\Order')->withPivot('number_of_product')->withTimestamps();
    }

    /**
     * @return LengthAwarePaginator
     */
    public function getAllResource()
    {
        $resources = $this->withTrashed()->latest()->paginate(5);
        return $resources;
    }

    /**
     * @param $id
     * @return Product|Product[]|Builder|Builder[]|Collection|Model|\Illuminate\Database\Query\Builder|\Illuminate\Database\Query\Builder[]
     */
    public function getResourceBy($id)
    {
        $resource = $this->withTrashed()->findOrFail($id);
        return $resource;
    }

    /**
     * @param $data
     * @return LengthAwarePaginator
     */
    public function getResourcesBy($data)
    {
        $name = $data['name'];
        $description = $data['description'];
        $category = $data['category'];
        $brand = $data['brand'];
        $softDelete = isset($data['softDelete']) ? $data['softDelete'][0] : null;

        if ($softDelete == null) {
            $resources = DB::table('products')
                ->join('images', 'products.id', '=', 'images.product_id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('images.status', '1')
                ->where('products.deleted_at', '=', null)
                ->where('products.name', 'like', '%' . $name . '%')
                ->where('products.description', 'like', '%' . $description . '%')
                ->where('brands.name', 'like', '%' . $brand . '%')
                ->where('categories.name', 'like', '%' . $category . '%')
                ->select(['brands.name AS brand_name', 'categories.name AS category_name', 'products.*', 'images.name AS image_name'])
                ->latest('products.created_at')
                ->paginate(5);
        }
        if ($softDelete != null) {
            $resources = DB::table('products')
                ->join('images', 'products.id', '=', 'images.product_id')
                ->join('brands', 'products.brand_id', '=', 'brands.id')
                ->join('categories', 'products.category_id', '=', 'categories.id')
                ->where('images.status', '1')
                ->where('products.deleted_at', '<>', null)
                ->where('products.name', 'like', '%' . $name . '%')
                ->where('products.description', 'like', '%' . $description . '%')
                ->where('brands.name', 'like', '%' . $brand . '%')
                ->where('categories.name', 'like', '%' . $category . '%')
                ->select(['brands.name AS brand_name', 'categories.name AS category_name', 'products.*', 'images.name AS image_name'])
                ->latest('products.created_at')
                ->paginate(5);
        }

        return $resources;
    }

    /**
     * function check number of product and number sell
     * @param $id
     * @param $numberSell
     * @return bool
     */
    public function checkNumberOf($id, $numberSell)
    {
        $product = $this->findOrFail($id);
        $oldNumber = $product->number_of;
        if($oldNumber >= $numberSell) {
            return true;
        }else
            return false;
    }

    /**
     * function update number of product before sell
     * @param $id
     * @param $numberSell
     * @return void
     */
    public function updateNumberOf($id, $numberSell)
    {
        $product = $this->findOrFail($id);
        $oldNumber = $product->number_of;
        $product->update(['number_of' => $oldNumber - $numberSell]);
    }

    /**
     * function get arr top seller product
     * @param $limit
     * @return \Illuminate\Support\Collection
     */
    public function topSeller($limit)
    {
        $topSeller = DB::table('order_product')
            ->join('products', 'product_id', '=', 'products.id')
            ->join('images', 'products.id', '=', 'images.product_id')
            ->where('images.status', '=', 1)
            ->select('products.name', 'products.price', 'images.name AS image_name', DB::raw('SUM(number_of_product) as number_sell'))
            ->groupBy('products.name')
            ->groupBy('image_name')
            ->groupBy('price')
            ->latest('number_sell')
            ->take($limit)
            ->get();
        return $topSeller;
    }
}
