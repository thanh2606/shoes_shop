<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = [
        'name',
        'description'
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function getAll()
    {
        return Category::latest()->paginate(5);
    }

    public function getCategoriesBy($data)
    {
        $name = isset($data['name']) ? $data['name'] : null;
        $description = isset($data['description']) ? $data['description'] : null;
        $categories = Category::Where('name', 'like', '%' . $name . '%')->where('description', 'like', '%' . $description . '%')->latest()->paginate(5);

        return $categories;
    }
}
