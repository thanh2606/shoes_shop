<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = [
        'name',
        'telephone_number',
        'address',
        'note'
    ];

    public function order() {
        return $this->hasMany('App\Order');
    }

    /**
     * @return mixed
     */
    public function getAllResource()
    {
        return $this->latest()->paginate(5);
    }

    /**
     * @param $data
     * @return mixed
     */
    public function getCustomersBy($data)
    {
        $name = isset($data['name']) ? $data['name'] : null;
        $telephone_number = isset($data['telephone_number']) ? $data['telephone_number'] : null;
        $address = isset($data['address']) ? $data['address'] : null;
        $note = isset($data['note']) ? $data['note'] : null;
        $customers = Customer::Where('name', 'like', '%' . $name . '%')->where('telephone_number', 'like', '%' . $telephone_number . '%')->Where('address', 'like', '%' . $address . '%')->Where('note', 'like', '%' . $note . '%')->latest()->paginate(5);

        return $customers;
    }
}
