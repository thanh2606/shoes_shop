<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    protected $fillable = [
        'customer_id',
        'note',
        'total_cost',
    ];

    public function customer()
    {
        return $this->belongsTo('App\Customer');
    }

    public function products()
    {
        return $this->belongsToMany('App\Product')->withPivot('number_of_product')->withTimestamps();
    }

    public function getAllResource()
    {
        return $this->latest()->paginate(5);
    }

    /**
     * function attach of $order with arr product
     * @param $order
     * @param $products
     */
    public function attachPivotWith($order, $products)
    {
        foreach ($products as $product) {
            $order->products()->attach($product['id'], [
                'number_of_product' => $product['number_of'],
            ]);
        }
    }

    /**
     * function detach of $order with arr product
     * @param $order
     * @param $products
     */
    public function detachPivotWith($order, $products)
    {
        foreach ($products as $product) {
            $order->products()->detach($product->id);
        }
    }

    /**
     * @param $arr
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function getResourcesBy($arr)
    {
        $name = isset($arr['name']) ? $arr['name'] : null;
        $note = isset($arr['note']) ? $arr['note'] : null;
        $resources = DB::table('orders')
            ->join('customers', 'customers.id', '=', 'orders.customer_id')
            ->where('customers.name', 'like', '%' . $name . '%')
            ->where('orders.note', 'like', '%' . $note . '%')
            ->latest('orders.created_at')
            ->paginate(5);

        return $resources;
    }

    /**
     * @param $day
     * @return mixed
     */
    public function getOrdersOn($day)
    {
        return $this->whereDate('created_at', $day)->latest()->get();
    }
}
