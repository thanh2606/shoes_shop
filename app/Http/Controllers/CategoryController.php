<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\CategoryRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class CategoryController extends Controller
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $categories = $this->category->getAll();

        return view('admins.category', ['categories' => $categories]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param CategoryRequest $request
     * @return JsonResponse
     */
    public function store(CategoryRequest $request)
    {
        $data = $this->category->create($request->all());
        return response()->json([
            'statusCode' => '200',
            'message' => 'created successful',
            'data' => $data
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param CategoryRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(CategoryRequest $request, $id)
    {
        $resource = Category::findOrFail($id);
        $resource->update($request->all());
        return response()->json([
            'statusCode' => '200',
            'message' => 'created successful',
            'data' => $resource
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $resource = Category::destroy($id);
        return response()->json([
            'statusCode' => 200,
            'message' => 'Delete successful',
            'data' => $resource
        ], 200);
    }

    /**
     * Display a listing of the resource by name and category.
     *
     * @param Request $request
     * @return Factory|View
     */
    public function search(Request $request)
    {
        $request->flash();
        $categories = $this->category->getCategoriesBy($request->all());
        return view('admins.category', [
            'categories' => $categories,
        ]);
    }
}
