<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Customer;
use App\Order;
use App\Product;
use Carbon\Carbon;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;

class OverviewController extends Controller
{
    protected $product;
    protected $order;

    public function __construct(Product $product, Order $order)
    {
        $this->product = $product;
        $this->order = $order;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|\Illuminate\View\View
     */
    public function index()
    {
        $topProductSeller = $this->product->topSeller(5);
        $brands = Brand::all();
        $products = Product::all();
        $customers = Customer::all();
        $categories = Category::all();
        $orders = $this->order->getOrdersOn(Carbon::today());
        return view('admins.overview', [
            'categories' => $categories,
            'products' => $products,
            'customers' => $customers,
            'brands' => $brands,
            'topProductSeller' => $topProductSeller,
            'orders' => $orders
        ]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function getResourcesOn(Request $request)
    {
        $day = strtotime($request->day);
        $day = date('Y-m-d',$day);
        $resources = $this->order->getOrdersOn($day);
        return response()->json([
            'statusCode' => 200,
            'message' => 'Get all order on day successful',
            'data' => $resources
        ], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
