<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Http\Requests\BrandRequest;
use App\Http\Traits\ImageTrait;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class BrandController extends Controller
{
    use ImageTrait;

    protected $brand;

    public function __construct(Brand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $brands = $this->brand->getAll();
        return view('admins.brand', ['brands' => $brands]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param BrandRequest $request
     * @return JsonResponse
     */
    public function store(BrandRequest $request)
    {
        $data = array();
        $data['name'] = $request->input('name');
        $data['description'] = $request->input('description');
        $data['email'] = $request->input('email');
        $image = $request->file('image');
        $data['image'] = $this->uploadImage($image);

        $response = $this->brand->create($data);

        return response()->json([
            'statusCode' => '200',
            'message' => 'created successful',
            'data' => $response,
            'pathImage' => asset('storage/' . $data['image'])
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param BrandRequest $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $resource = Brand::findOrFail($id);
        if ($this->deleteImageHas($resource->image)) {
            $resource->delete();
            return response()->json([
                'statusCode' => '200',
                'message' => 'Delete successful',
                'data' => $resource
            ], 200);
        } else
            return response()->json([
                'statusCode' => '404',
                'message' => 'Not fourd image of resource',
                'data' => $resource
            ], 404);
    }

    /**
     * Edit brand with ajax.
     * @param $id
     * @return JsonResponse
     */
    public function editWith($id, BrandRequest $request)
    {
        $data = array();
        $resource = Brand::findOrFail($id);
        if ($request->hasFile('image')) {
            $this->deleteImageHas($resource->image);
            $data['image'] = $this->uploadImage($request->file('image'));
            $pathImage = asset('storage/' . $data['image']);
        } else $pathImage = asset('storage/' . $resource->image);
        $data['name'] = $request->input('name');
        $data['description'] = $request->input('description');
        $data['email'] = $request->input('email');
        $resource->update($data);

        return response()->json([
            'statusCode' => '200',
            'message' => 'Created successful',
            'data' => $resource,
            'pathImage' => $pathImage
        ], 200);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function search(Request $request)
    {
        $request->flash();
        $brands = $this->brand->getBrandsBy($request->all());
        return view('admins.brand', ['brands' => $brands]);
    }
}
