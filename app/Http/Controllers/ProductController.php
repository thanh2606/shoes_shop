<?php

namespace App\Http\Controllers;

use App\Brand;
use App\Category;
use App\Http\Requests\ProductRequest;
use App\Http\Traits\ImageTrait;
use App\Image;
use App\Product;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\View\View;

class ProductController extends Controller
{
    use ImageTrait;

    protected $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    /**
     * @return Factory|View
     */
    public function index()
    {
        $resources = $this->product->getAllResource();
        return view('admins.product.product', ['resources' => $resources]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $categories = Category::all();
        $brands = Brand::all();
        return view('admins.product.createProduct', ['categories' => $categories, 'brands' => $brands]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(ProductRequest $request)
    {
        $dataProduct = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'number_of' => $request->input('number_of'),
            'price' => $request->input('price'),
            'category_id' => $request->input('category'),
            'brand_id' => $request->input('brand'),
        ];
        $response = $this->product->create($dataProduct);

        $dataImage = [];
        $dataImage['product_id'] = $response->id;
        $mainImage = $request->input('mainImage');
        $dataImage['imagesUploaed'] = $this->uploadMultiImageWith($request->file('images'), $mainImage);
        if (isset($dataImage['imagesUploaed']['nomarImage']))
            foreach ($dataImage['imagesUploaed']['nomarImage'] as $nomarImage => $value) {
                $dataNewEmage = [
                    'name' => $value,
                    'product_id' => $dataImage['product_id'],
                    'status' => 0
                ];
                Image::create($dataNewEmage);
            }
        $dataNewMainEmage = [
            'name' => $dataImage['imagesUploaed']['mainImage'][0],
            'product_id' => $dataImage['product_id'],
            'status' => 1
        ];
        Image::create($dataNewMainEmage);

        session(['succesful' => 'Create the product succesful']);
        return redirect()->back()->withInput();
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $categories = Category::all();
        $brands = Brand::all();
        $resource = $this->product->getResourceBy($id);
        return view('admins.product.editProduct', ['idProduct' => $id, 'resource' => $resource, 'categories' => $categories, 'brands' => $brands]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return RedirectResponse
     */
    public function update(ProductRequest $request, $id)
    {
        $product = $this->product->findOrFail($id);
        $product->update($request->all());
        session(['successful' => 'Update the product successful']);
        return redirect()->back()->withInput();
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $product = $this->product->findOrFail($id);
        $product->delete();
        return response()->json([
            'statusCode' => '200',
            'message' => 'Soft delete successful',
            'data' => $product
        ], 200);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function realDelete($id)
    {
        $product = $this->product->findOrFail($id);
        $images = Image::where('product_id', $id)->get('name');
        $deleteImages = $this->deleteMultiImageIn($images);
        if ($deleteImages) {
            $product->forceDelete();
            return response()->json([
                'statusCode' => '200',
                'message' => 'Real delete successful',
                'data' => $product
            ], 200);
        } else
            return response()->json([
                'statusCode' => '500',
                'message' => 'Real delete is fail',
                'data' => $product
            ], 500);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function search(Request $request)
    {
        $request->flash();
        $resources = $this->product->getResourcesBy($request->all());
        return view('admins.product.product', ['resources' => $resources]);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function restore($id)
    {
        $product = $this->product->withTrashed()->findOrFail($id);
        $product->restore();
        return response()->json([
            'statusCode' => '200',
            'message' => 'Restore successful',
            'data' => $product
        ], 200);
    }
}
