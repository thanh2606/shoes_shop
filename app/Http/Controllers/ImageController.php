<?php

namespace App\Http\Controllers;

use App\Http\Traits\ImageTrait;
use App\Image;
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ImageController extends Controller
{
    use ImageTrait;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * update new main image and old main image
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function updateImages(Request $request, $id)
    {
        $image = Image::findOrFail($id);
        $image->update(['status' => $request->status]);
        $oldMainImage = Image::findOrFail($request->oldMainImage);
        $oldMainImage->update(['status' => 0]);
        return response()->json([
            'statusCode' => 200,
            'message' => 'Edit image succesful',
            'data' => $image
        ],200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $image = Image::findOrFail($id);
        $isDeletedImage = $this->deleteImageHas($image->name);
        $image->delete();
        if ($isDeletedImage){
            return response()->json([
                'statusCode' => 200,
                'message' => 'Delete image succesful',
                'data' => $image
            ],200);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function postImage(Request $request)
    {
        $product_id = $request->product_id;
        $arrImg = [];
        $arrNameImg = $this->uploadMultiImageWith($request->file('images'), null);
        $i = 0;
        foreach ($arrNameImg as $image) {
            $data = [
                'product_id' => $product_id,
                'name' => $image,
                'status' => 0
            ];
            $arrImg[$i] = Image::create($data);
            $i++;
        }
        return response()->json([
            'statusCode' => 200,
            'message' => 'Delete image succesful',
            'data' => $arrImg
        ],200);
    }
}
