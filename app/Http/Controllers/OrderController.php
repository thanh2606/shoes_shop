<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Order;
use App\Product;
use App\Rules\IsntZero;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\View;

class OrderController extends Controller
{
    protected $order;
    protected $product;

    public function __construct(Order $order, Product $product)
    {
        $this->order = $order;
        $this->product = $product;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|View
     */
    public function index()
    {
        $resources = $this->order->getAllResource();
        return view('admins.orders.order', ['resources' => $resources]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Factory|View
     */
    public function create()
    {
        $customers = Customer::latest()->get();
        $products = Product::latest()->get();
        return view('admins.orders.createAndUpdate', ['customers' => $customers, 'products' => $products]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'customer' => 'required',
            'total_cost' => ['required', 'numeric', new IsntZero()],
            'products' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'statusCode' => 500,
                'message' => 'Create order fail',
                'data' => $validator->errors()
            ], 500);
        } else {
            $orderData = [
                'customer_id' => $request->customer['id'],
                'note' => $request->note,
                'total_cost' => $request->total_cost
            ];
            $products = $request->products;
            $stringResponse = '';
            $check = false;
            foreach ($products as $product) {
                if (!$this->product->checkNumberOf($product['id'], $product['number_of'])) {
                    $check = false;
                    $stringResponse = 'The product has name ' . $product['name'] . ' not enough to sell';
                    break;
                } else $check = true;
            }
            if ($check) {
                foreach ($products as $product) {
                    $this->product->updateNumberOf($product['id'], $product['number_of']);
                }
                $order = $this->order->create($orderData);
                $this->order->attachPivotWith($order, $products);
                return response()->json([
                    'statusCode' => 200,
                    'message' => 'Create order successful',
                ], 200);
            } else
                return response()->json([
                    'statusCode' => 500,
                    'message' => $stringResponse
                ], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return Factory|View
     */
    public function edit($id)
    {
        $customers = Customer::latest()->get();
        $products = Product::latest()->get();
        $order = $this->order->findOrFail($id);
        return view('admins.orders.createAndUpdate', ['customers' => $customers, 'products' => $products, 'order' => $order]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param int $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'total_cost' => ['required', 'numeric', new IsntZero()],
            'products' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'statusCode' => 500,
                'message' => 'Create order fail',
                'data' => $validator->errors()
            ], 500);
        } else {
            $orderData = [
                'note' => $request->note,
                'total_cost' => $request->total_cost
            ];
            $order = $this->order->findOrFail($id);
            $oldProductArr = $order->products()->get();
            foreach ($oldProductArr as $product) {
                $inventory = $this->product->findOrFail($product->id)->number_of;
                //update number of product : oldNumberOf + ( number of in order_product)
                $product->update(['number_of' => $inventory + $product->pivot->number_of_product]);
            }
            $this->order->detachPivotWith($order, $oldProductArr);
            $check = true;
            $stringResponse = '';
            $newProductArr = $request->products;
            foreach ($newProductArr as $product) {
                if (!$this->product->checkNumberOf($product['id'], $product['number_of'])) {
                    $check = false;
                    $stringResponse = 'The product has name ' . $product['name'] . ' not enough to sell';
                    break;
                }
            }
            if ($check) {
                foreach ($newProductArr as $product) {
                    $this->product->updateNumberOf($product['id'], $product['number_of']);
                }

                $this->order->attachPivotWith($order, $newProductArr);
                $order->update($orderData);
                return response()->json([
                    'statusCode' => 200,
                    'message' => 'Update order successful',
                    'data' => $order
                ], 200);
            } else {
                foreach ($oldProductArr as $product) {
                    $oldNumberOf = $product->number_of;
                    //update number of product : oldNumberOf - ( number of in order_product)
                    $product->update(['number_of' => $oldNumberOf - $product->pivot->number_of_product]);
                }
                return response()->json([
                    'statusCode' => 500,
                    'message' => $stringResponse
                ], 500);
            }
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        $order= $this->order->destroy($id);
        return response()->json([
            'statusCode' => 200,
            'message' => 'Delete order successful',
            'data' => $order
        ], 200);
    }

    /**
     * @param Request $request
     * @return Factory|View
     */
    public function search(Request $request)
    {
        $request->flash();
        $resources = $this->order->getResourcesBy($request->all());
        return view('admins.orders.order', ['resources' => $resources]);
    }
}
