<?php

namespace App\Http\Controllers;

use App\Customer;
use App\Http\Requests\CustomerRequest;
use Illuminate\Contracts\View\Factory;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    protected $customer;

    public function __construct(Customer $category)
    {
        $this->customer = $category;
    }

    /**
     * Display a listing of the resource.
     *
     * @return Factory|\Illuminate\View\View
     */
    public function index()
    {
        $customers = $this->customer->getAllResource();
        return view('admins.customer', ['customers' => $customers]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CustomerRequest $request)
    {
        $newCustomer = $this->customer->create($request->all());
        return response()->json([
            'statusCode' => 200,
            'message' => 'Create new customer successful',
            'data' => $newCustomer
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CustomerRequest $request, $id)
    {
        $customer = $this->customer->findOrFail($id);
        $customer->update($request->all());
        return response()->json([
            'statusCode' => 200,
            'message' => 'Update the customer successful',
            'data' => $customer
        ], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id)
    {
        $customer = $this->customer->destroy($id);
        return response()->json([
            'statusCode' => 200,
            'message' => 'Delete the customer successful',
            'data' => $customer
        ], 200);
    }

    public function search(Request $request)
    {
        $request->flash();
        $customers = $this->customer->getCustomersBy($request->all());
        return view('admins.customer', ['customers' => $customers]);
    }
}
