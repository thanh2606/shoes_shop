<?php

namespace App\Http\Requests;

use http\Env\Request;
use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if (\Illuminate\Http\Request::route()->getName() == 'products.update'){
            return [
                'category' => 'required',
                'brand' => 'required',
                'name' => ['required', 'min:2', 'unique:products,name,' . $this->product],
                'description' => 'required',
                'number_of' => ['required', 'integer'],
                'price' => ['required', 'integer'],
            ];
        }
        return [
            'category' => 'required',
            'brand' => 'required',
            'name' => ['required', 'min:2', 'unique:products,name,' . $this->product],
            'description' => 'required',
            'number_of' => ['required', 'integer'],
            'price' => ['required', 'integer'],
            'images.*' => ['required', 'image', 'mimes:jpeg,png,jpg,gif', 'max:2048'],
            'images' => ['required']
        ];
    }

    public function messages()
    {
        return [
            'category.required' => 'Category is required',
            'brand.required' => 'Brand is required',
            'name.required' => 'Name is required',
            'name.unique' => 'Name already exists',
            'name.min' => 'Minimum 2 character',
            'description.required' => 'Description is required',
            'number_of.requied' => 'Number of product is required',
            'number_of.iteger' => 'Number of product must be integer',
            'images.*.required' => 'Image must required',
            'images.required' => 'Image must required'
        ];
    }
}
