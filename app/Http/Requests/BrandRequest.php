<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class BrandRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'min:2', 'unique:brands,name,' . $this->id],
            'description' => 'required',
            'email' => 'required|email:rfc,dns',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Name is required',
            'name.unique' => 'Name must unique',
            'name.min' => 'Minimum 2 character',
            'description.required' => 'Description is required',
            'email.email' => 'Email must like example@example.com',
            'image.required' => 'Image must required'
        ];
    }
}
