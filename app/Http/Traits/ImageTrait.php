<?php

namespace App\Http\Traits;

use Illuminate\Support\Facades\Storage;

trait ImageTrait
{
    /**
     * @param $image
     * @return mixed
     */
    public function uploadImage($image)
    {
        $path = $image->store('categories', 'public');
        return $path;
    }

    /**
     * @param $nameImage
     * @return bool
     */
    public function deleteImageHas($nameImage)
    {
        if (Storage::disk('public')->exists($nameImage)) {
            Storage::delete($nameImage);
            return true;
        } else
            return false;
    }

    /**
     * @param $images
     * @return bool
     */
    public function deleteMultiImageIn($images)
    {
        foreach ($images as $image) {
            if (Storage::disk('public')->exists($image->name)) {
                Storage::delete($image->name);
            } else
                return false;
        }
        return true;
    }

    /**
     * @param $arrImage
     * @return array
     */
    public function uploadMultiImageWith($arrImage, $mainImage)
    {
        $i = 0;
        $kq = [];
        if ($mainImage != null)
            foreach ($arrImage as $key => $value) {
                if ($i != $mainImage)
                    $kq['nomarImage'][$i] = $value->store('products', 'public');
                if ($i == $mainImage)
                    $kq['mainImage'][0] = $value->store('products', 'public');
                $i++;
            }
        else
            foreach ($arrImage as $key => $value) {
                $kq[$i] = $value->store('products', 'public');
                $i++;
            }
        return $kq;
    }
}
