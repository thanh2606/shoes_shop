<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $fillable = [
        'name',
        'description',
        'email',
        'image'
    ];

    public function products()
    {
        return $this->hasMany('App\Product');
    }

    public function getAll()
    {
        return $this->latest()->paginate(5);
    }

    public function getBrandsBy($data)
    {
        $name = isset($data['name']) ? $data['name'] : null;
        $description = isset($data['description']) ? $data['description'] : null;
        $email = isset($data['email']) ? $data['email'] : null;
        $brands = Brand::where('name', 'like', '%'.$name.'%')->where('description', 'like', '%'.$description.'%')->where('email', 'like', '%'.$email.'%')->latest()->paginate(5);
        return $brands;
    }
}
