<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name('client.index');

//login
Route::get('login', 'UserController@login')->name('login');
Route::post('login', 'UserController@checkLogin')->name('login.post');
Route::get('logout', 'UserController@logout')->name('logout');
Route::patch('settingUser/{id}', 'UserController@update')->middleware('login')->name('user.update');

Route::group(['prefix' => 'admin', 'middleware' => 'login'], function () {
    //Overview
    Route::get('/', 'OverviewController@index')->name('overview');

    //Category
    Route::resource('categories', 'CategoryController');
    Route::get('searchCategories', 'CategoryController@search')->name('category.search');

    //Brand
    Route::resource('brands', 'BrandController');
    Route::post('brands/{id}', 'BrandController@editWith')->name('brands.delete');
    Route::get('searchBrands', 'BrandController@search')->name('brands.search');

    //product
    Route::resource('products', 'ProductController');
    Route::get('searchProducts', 'ProductController@search')->name('products.search');
    Route::post('products/restore/{id}', 'ProductController@restore')->name('products.restore');
    Route::post('products/realDelete/{id}', 'ProductController@realDelete')->name('products.realDelete');

    //image
    Route::resource('images', 'ImageController');
    Route::post('images/updateImages/{id}', 'ImageController@updateImages')->name('images.update');
    Route::post('images/uploadMultiImage', 'ImageController@postImage')->name('images.uploadMultiImage');

    //customer
    Route::resource('customers', 'CustomerController');
    Route::get('searchCustomers', 'CustomerController@search')->name('customers.search');

    //order
    Route::resource('orders', 'OrderController');
    Route::get('searchOrders', 'OrderController@search')->name('orders.search');

    //overview
    Route::post('overview/getOrdersOnDay', 'OverviewController@getResourcesOn')->name('overview.getOnDay');

});
