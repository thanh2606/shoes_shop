//variable
var idEdit;
var idDelete;

//function clear message error when open modal
$('.createResource').click(function () {
    $('.error').text('');
});

//function append table
function prepend(response) {
    let tbody = $('.tbody');
    let name = response.data.name;
    let created_at = response.data.created_at;
    let telephone_number = response.data.telephone_number;
    let address = response.data.address;
    let note = response.data.note;
    let id = response.data.id;
    let tr = `<tr>
                                        <th class="stt"></th>
                                        <td>
                                            <a id="tr-name-${id}">${name}</a>
                                            <br/>
                                            <small>Created ${created_at}</small>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="tr-telephone_number-${id}">${telephone_number}</p>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="tr-address-${id}">${address}</p>
                                            </ul>
                                        </td>
                                        <td>
                                            <ul class="list-inline">
                                                <p id="tr-note-${id}">${note}</p>
                                            </ul>
                                        </td>
                                        <td class="tdEdit">
                                            <button type="button" class="editBtn btn btn-info btn-xs"
                                                    data-toggle="modal" data-target=".bs-example-modal-lg1"
                                                    data_name="${name}"
                                                    data_telephone_number="${telephone_number}"
                                                    data_address="${address}"
                                                    data_note="${note}"
                                                    data_id="${id}" id="btnEdit${id}"><i
                                                    class="fa fa-pencil"></i> Edit
                                            </button>
                                            <button type="button" class="deleteBtn btn btn-danger btn-xs"
                                                    data_id="${id}"><i class="fa fa-trash-o"></i> Delete
                                            </button>
                                        </td>
                                    </tr>`;
    tbody.prepend(tr);
}

//function reload tr of tableBody
function reloadOrderOfTableBody() {
    $("tr .stt").each(function (i) {
        $(this).text(i + 1);
        i++;
    });
}

//CREATE CUSTOMER
$('#postResource').click(function (event) {
    event.preventDefault();
    let dataResource = $('#createResourceForm').serialize();
    let urlResource = '/admin/customers';
    callAjax(urlResource, 'post', dataResource)
        .done(response => {
            Swal.fire({
                icon: 'success',
                title: 'OK',
                text: 'Create customer successful',
            }).then((result) => {
                if (result.value) {
                    prepend(response);
                    reloadOrderOfTableBody();
                    if ($('tbody tr').length == 6) {
                        $('tbody tr:last-child').remove();
                    }
                }
            })
        })
        .fail(error => {
            show_error_and_focus_on_form('-create-form', error.responseJSON.errors);
            showErrorMessage('Opp!', 'Create category fail');
        })
});

//EDIT CUSTOMER
//load data resource to form edit
$(document).on('click', '.editBtn', function (event) {
    $('.error').text('');
    let name = $(this).attr('data_name');
    let telephone_number = $(this).attr('data_telephone_number');
    let address = $(this).attr('data_address');
    let note = $(this).attr('data_note');
    let id = $(this).attr('data_id');
    $('#input-name-edit-form').val(name);
    $('#input-telephone_number-edit-form').val(telephone_number);
    $('#input-address-edit-form').val(address);
    $('#input-note-edit-form').text(note);
    idEdit = $(this).attr('data_id');
});

//call ajax to edit
$(document).on('click', '#postEditCategory', function (event) {
    event.preventDefault();
    let dataResource = $('#editResourceForm').serialize();
    let method = 'patch';
    let url = '/admin/customers/' + idEdit;
    callAjax(url, method, dataResource)
        .done(response => {
            Swal.fire({
                icon: 'success',
                title: 'OK',
                text: response.message,
            }).then((result) => {
                if (result.value) {
                    reloadTrWith(response.data, idEdit);
                    reloadBtnWith(response.data, idEdit);
                }
            })
        })
        .fail(error => {
            show_error_and_focus_on_form('-edit-form', error.responseJSON.errors);
            showErrorMessage('Opp!', 'Update customer is fail');
        })
});

//DELETE CUSTOMER
$(document).on('click', '.deleteBtn', function (event) {
    event.preventDefault();
    let method = 'delete';
    idDelete = $(this).attr('data_id');
    let url = '/admin/customers/' + idDelete;
    Swal.fire({
        icon: 'question',
        title: 'warring',
        text: 'The customer will be destroyed. Are you sure?',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value)
            callAjax(url, method, null)
                .done(response => {
                    Swal.fire({
                        icon: 'success',
                        title: 'OK',
                        text: response.message,
                    }).then((result) => {
                        if (result.value) {
                            $(this).parent().parent().remove();
                        }
                    })
                })
                .fail(error => {
                    showErrorMessage('Opp!', 'Delete customer is fail');
                })
    })
});
